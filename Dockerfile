FROM maven:3.3.9-jdk-8-alpine

ENV VERSION=1.25.0

RUN apk add --update --no-cache jq bash curl wget maven git

RUN wget https://dl.bintray.com/jfrog/jfrog-cli-go/$VERSION/jfrog-cli-linux-386/jfrog

RUN chmod +x jfrog && mv jfrog /usr/bin/

COPY pipe /usr/bin/

ENTRYPOINT ["/usr/bin/pipe.sh"]
